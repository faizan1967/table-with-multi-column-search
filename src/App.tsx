import * as React from "react";
import "antd/dist/antd.css";

import TableWithMultiColumnSearch from "./Table";

const mockData = [
  {
    collector_id: "1235",
    collector_ip: "192.168.1.1",
    exporter_ip: "192.168.1.1",
    status: "bad",
    condition: "running",
  },
  {
    collector_id: "1235",
    collector_ip: "192.168.1.1",
    exporter_ip: "192.168.2.1",
    status: "good",
    condition: "paused",
  },
  {
    collector_id: "1235",
    collector_ip: "192.168.1.1",
    exporter_ip: "192.168.4.6",
    status: "warning",
    condition: "running",
  },
  {
    collector_id: "1235",
    collector_ip: "192.168.1.1",
    exporter_ip: "192.168.23.1",
    status: "unknown",
    condition: "unknown",
  },
  {
    collector_id: "1235",
    collector_ip: "192.168.1.1",
    exporter_ip: "192.168.1.100",
    status: "good",
    condition: "running",
  },
];

const filterColumns = ["exporter_ip", "status", "condition"];

const columns = filterColumns.map((item, index) => ({
  id: `${index}-${item}`,
  columnName: item,
}));

const columnBaseData = columns.reduce((obj, item) => {
  const columnData = mockData.map(
    (dataItem) => dataItem[item.columnName as keyof typeof mockData[number]]
  );
  const uniqueColumnData = Array.from(new Set(columnData));

  return {
    ...obj,
    [item.id]: uniqueColumnData.map((columnItem) => ({
      id: `${item.id}`,
      columnValue: columnItem,
    })),
  };
}, {});

export default function App() {
  return (
    <div className="App">
      <h1>Table with multi-column-search demo (created by faizan tanveer)</h1>
      <TableWithMultiColumnSearch
        columns={columns}
        columnBaseData={columnBaseData}
        tableData={{ data: mockData }}
      />
    </div>
  );
}
