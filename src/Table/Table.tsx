import React, { useState, useEffect } from "react";
import { Table as AntdTable } from "antd";

import { TableData, TableProps } from "./Table.types";
import { removeUnderScore } from "./Table.utils";

import { EmptyTextStyled } from "./Table.styles";

function EmptyDataText() {
  return (
    <EmptyTextStyled>
      <p className="description">Sorry, Your search produced no result.</p>
    </EmptyTextStyled>
  );
}

export default function Table<T>({ data = [] }: TableProps<T>) {
  type ColumnKeys = keyof TableData<T>[number];
  const tableData = data !== null ? data : [];
  const [tableColumns, setTableColumns] = useState<any[]>();
  const dataSource = tableData.map((row, index) => ({
    ...row,
    key: index.toString(),
  }));
  const columnKeys = tableData.length
    ? (Object.keys(tableData[0]) as Array<ColumnKeys>)
    : [];

  const columns = tableData.length
    ? columnKeys.map((key) => ({
        title: removeUnderScore(key as string),
        dataIndex: key,
        key,
      }))
    : [];

  useEffect(() => {
    if (tableData.length) {
      setTableColumns(columns);
    }
  }, [tableData?.length]);

  return (
    <AntdTable
      bordered
      columns={tableColumns}
      dataSource={dataSource}
      locale={{ emptyText: <EmptyDataText /> }}
    />
  );
}
