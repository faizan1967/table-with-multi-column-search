import { Dispatch, SetStateAction } from "react";

export type TableData<T> = {
  [key in keyof T]: T[key];
}[];

export interface TableProps<T = any> {
  data: TableData<T>;
}

export interface TableWithMultiColumnSearchProps {
  columnBaseData: { [key: string]: { columnValue: string; id: string }[] };
  columns: { id: string; columnName: string }[];
  tableData: TableProps;
}

export interface TableWithMultiColumnSearchState {
  selectedColumns: { id: string; columnName: string; columnValue?: string }[];
  selectedId?: string;
  currentMode?: "column-title" | "column-data";
  inputText: string;
  tableData: TableProps["data"];
  dropdownLeftSpace: number;
}

export interface MultiColumnSearchDropDownMenuProps
  extends TableWithMultiColumnSearchProps {
  state: TableWithMultiColumnSearchState;
  setState: Dispatch<SetStateAction<TableWithMultiColumnSearchState>>;
}
