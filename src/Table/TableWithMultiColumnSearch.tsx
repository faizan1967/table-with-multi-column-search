import React, {
  Dispatch,
  SetStateAction,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import { Button, Dropdown, Input } from "antd";

import { crossIcon, filterIcon } from "./svg";
import {
  CustomMenuStyled,
  SelectedItemsRendrerStyled,
  TableWithMultiColumnSearchStyled,
} from "./Table.styles";
import {
  MultiColumnSearchDropDownMenuProps,
  TableWithMultiColumnSearchProps,
  TableWithMultiColumnSearchState,
} from "./Table.types";
import Table from "./Table";
import { getMultiColumnFilteredTableData } from "./Table.utils";

function ColumnNamesRenderer(props: MultiColumnSearchDropDownMenuProps) {
  const { setState, state, columns } = props;

  const onColumnSelection = useCallback(
    (column) => {
      setState((st) => ({
        ...st,
        selectedId: column.id,
        currentMode: "column-data",
        inputText: "",
        selectedColumns: [
          ...st.selectedColumns,
          { columnName: column.columnName, id: column.id },
        ],
      }));
    },
    [state, columns]
  );

  const filteredColumns = useMemo(() => {
    return columns
      .filter((column) =>
        state.selectedColumns.every(
          (selectedColumn) => selectedColumn.id !== column.id
        )
      )
      .filter((unSelectedColumn) => {
        const lowerColumnName = unSelectedColumn.columnName
          .toString()
          .toLowerCase();
        const lowerInputText = state.inputText.toString().toLowerCase();
        return lowerColumnName.includes(lowerInputText);
      });
  }, [columns, state.selectedColumns, state.inputText]);

  return (
    <CustomMenuStyled>
      {filteredColumns.map((column, index) => (
        <div
          className="menu-item"
          key={`${column.id}-${index}`}
          onClick={() => onColumnSelection(column)}
          role="button"
        >
          {column.columnName}
        </div>
      ))}
    </CustomMenuStyled>
  );
}

function ColumnDataRenderer(props: MultiColumnSearchDropDownMenuProps) {
  const { setState, state, columnBaseData, columns } = props;

  const slectedData = useMemo(() => columnBaseData[state?.selectedId || ""], [
    state,
    columnBaseData,
    columns,
  ]);

  const onValueSelection = useCallback(
    (item) => {
      const selectedColumnName = columns.find((column) => column.id === item.id)
        ?.columnName;
      const index = state.selectedColumns.findIndex(
        (column) => column.id === item.id
      );
      const newData = [
        ...state.selectedColumns.slice(0, index),
        { ...item, columnName: selectedColumnName },
        ...state.selectedColumns.slice(index + 1),
      ];
      setState((st) => ({
        ...st,
        selectedColumns: newData,
        currentMode: "column-title",
        inputText: "",
      }));
    },
    [state, columnBaseData, columns]
  );

  const filteredSelectedData = useMemo(() => {
    return slectedData.filter((unSelectedColumn) => {
      const lowerColumnName = unSelectedColumn.columnValue
        .toString()
        .toLowerCase();
      const lowerInputText = state.inputText.toString().toLowerCase();
      return lowerColumnName.includes(lowerInputText);
    });
  }, [slectedData, state.inputText]);

  return (
    <CustomMenuStyled isColumnDataMode>
      {filteredSelectedData.map((column, index) => (
        <div
          className="menu-item"
          key={`${column.id}-${index}`}
          onClick={() => onValueSelection(column)}
          role="button"
        >
          {column.columnValue}
        </div>
      ))}
    </CustomMenuStyled>
  );
}

function SelectedItemsRendrer({
  listOfSelectedItems,
  setState,
}: {
  listOfSelectedItems: TableWithMultiColumnSearchState["selectedColumns"];
  setState: Dispatch<SetStateAction<TableWithMultiColumnSearchState>>;
}) {
  const onRemoveSelectedItems = useCallback(
    (id) => {
      const filterdList = listOfSelectedItems.filter((item) => item.id !== id);
      setState((st) => ({
        ...st,
        selectedColumns: filterdList,
      }));
    },
    [listOfSelectedItems, setState]
  );
  return (
    <SelectedItemsRendrerStyled
      className="selected-items-rendrer"
      id="selected-items"
    >
      {filterIcon()}
      {listOfSelectedItems.map((item) => (
        <div className="selected-columns-rendrer" key={item.id}>
          <div className="column-name">{item.columnName}</div>
          {item.columnValue && (
            <div className="column-value">
              {item.columnValue}{" "}
              <span
                onClick={() => onRemoveSelectedItems(item.id)}
                role="button"
              >
                {crossIcon("#777777", "8px", "8px")}
              </span>
            </div>
          )}
        </div>
      ))}
    </SelectedItemsRendrerStyled>
  );
}

function TableWithMultiColumnSearch(props: TableWithMultiColumnSearchProps) {
  const { tableData } = props;
  const [state, setState] = useState<TableWithMultiColumnSearchState>({
    currentMode: "column-title",
    selectedColumns: [],
    inputText: "",
    tableData: tableData.data,
    dropdownLeftSpace: 40,
  });

  useEffect(() => {
    const updatedTable = getMultiColumnFilteredTableData(
      state.selectedColumns,
      tableData.data
    );
    const selectedColumnsContainerWidth =
      document.getElementById("selected-items")?.offsetWidth || 0;
    const updatedLeftSpace = state.selectedColumns.length
      ? selectedColumnsContainerWidth + 27
      : selectedColumnsContainerWidth;

    setState((st) => ({
      ...st,
      tableData: updatedTable,
      dropdownLeftSpace: updatedLeftSpace,
    }));
  }, [tableData.data, state.selectedColumns]);

  const onChange = useCallback(
    (event) => {
      const inputText = event.target.value;

      setState((st) => ({ ...st, inputText }));
    },
    [state.inputText]
  );

  const onReset = useCallback(() => {
    setState((st) => ({
      ...st,
      selectedColumns: [],
      inputText: "",
      currentMode: "column-title",
    }));
  }, [state]);

  return (
    <TableWithMultiColumnSearchStyled
      showSuffix={Boolean(state.selectedColumns.length)}
      dropDownLeftSpace={state.dropdownLeftSpace}
    >
      <Dropdown
        overlay={
          state?.currentMode === "column-title" ? (
            <ColumnNamesRenderer {...props} state={state} setState={setState} />
          ) : (
            <ColumnDataRenderer {...props} state={state} setState={setState} />
          )
        }
        trigger={["click"]}
        getPopupContainer={(trigger) => trigger.parentNode as HTMLElement}
      >
        <Input
          prefix={
            <SelectedItemsRendrer
              listOfSelectedItems={state.selectedColumns}
              setState={setState}
            />
          }
          value={state.inputText}
          onChange={onChange}
          suffix={
            <Button onClick={onReset} type="link">
              {crossIcon("#777777")}
            </Button>
          }
        />
      </Dropdown>
      <Table {...tableData} data={state.tableData} />
    </TableWithMultiColumnSearchStyled>
  );
}

export default TableWithMultiColumnSearch;
