import styled from "styled-components";

export const EmptyTextStyled = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  min-height: 250px;
  .description {
    font-size: 13px;
    font-weight: normal;
    color: #777777;
    text-align: center;
  }
`;

export const TableWithMultiColumnSearchStyled = styled.div<{
  showSuffix?: boolean;
  dropDownLeftSpace: number;
}>`
  position: relative;
  .ant-input-affix-wrapper {
    padding: 0px 25px;
    border: none;
    border-bottom: 1px solid white;
    background: #fafbfc;
    .ant-input {
      background: inherit;
    }

    :hover {
      border-bottom-color: #00a7e1;
      .selected-items-rendrer {
        > svg {
          path {
            fill: #00a7e1;
          }
        }
      }
    }
    &.ant-input-affix-wrapper-focused {
      border-bottom-color: #00a7e1;
      box-shadow: none;
      outline: none;

      .selected-items-rendrer {
        > svg {
          path {
            fill: #00a7e1;
          }
        }
      }
    }

    .ant-input-suffix {
      display: ${(props) => !props.showSuffix && "none"};
      button {
        padding: 0;
        :hover {
          svg {
            path {
              fill: #00a7e1;
              stroke: #00a7e1;
            }
          }
        }
      }
    }
  }

  .ant-dropdown {
    min-width: fit-content !important;
    left: ${(props) => props.dropDownLeftSpace}px !important;
  }
`;

export const CustomMenuStyled = styled.div<{ isColumnDataMode?: boolean }>`
  background: white;
  color: #777777;
  box-shadow: 0 0 4px 0 rgba(119, 119, 119, 0.57);
  width: fit-content;
  max-height: 200px;
  overflow-y: auto;
  padding: 15px 0;
  .menu-item {
    display: flex;
    align-items: center;
    padding: ${(props) =>
      props.isColumnDataMode ? "2px 44.7px 2px 25px" : "2px 33px 2px 25px"};
    text-transform: capitalize;
    cursor: pointer;

    :hover {
      color: #000000;
      font-weight: 500;
    }
  }
`;

export const SelectedItemsRendrerStyled = styled.div`
  &.selected-items-rendrer {
    display: flex;
    align-items: center;

    > svg {
      margin: 11px;
    }
    .selected-columns-rendrer {
      display: flex;
      align-items: center;

      .column-name,
      .column-value {
        display: flex;
        align-items: center;
        height: 32px;
        font-size: 13px;
        padding: 8px 20px;
      }
      .column-name {
        color: #777777;
        background: #f1f5f6;

        height: 32px;
      }

      .column-value {
        background: #dde9ec;
        color: #000000;
        margin-right: 5px;

        svg {
          display: flex;
          align-items: center;
          margin-left: 14px;
          cursor: pointer;
        }
      }
    }
  }
`;
