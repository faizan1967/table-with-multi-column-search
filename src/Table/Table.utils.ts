import { TableProps, TableWithMultiColumnSearchState } from "./Table.types";

export function removeUnderScore(value: string): string {
  return value.replace(/_/g, " ");
}

export function getMultiColumnFilteredTableData(
  selectedColumns: TableWithMultiColumnSearchState["selectedColumns"],
  tableData: TableProps["data"]
) {
  const filteredSelectedColumns = selectedColumns.filter(
    (selectedColumn) => selectedColumn.columnValue
  );

  return tableData.filter((dataItem) =>
    filteredSelectedColumns.every(
      (item) => dataItem[item.columnName] === item.columnValue
    )
  );
}
